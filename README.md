# Renovate Bot

## Configure CI/CD variables

You need to add a GitLab [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token) (scopes: `read_user`, `api` and `write_repository`) as `RENOVATE_TOKEN` to CI/CD variables.
You can also use a GitLab [Group Access Token](https://docs.gitlab.com/ee/user/group/settings/group_access_tokens.html).
Checkout Renovate platform [docs](https://docs.renovatebot.com/modules/platform/gitlab/) and #53 for more information.

It is also recommended to configure a [GitHub.com Personal Access Token](https://docs.github.com/en/free-pro-team@latest/github/authenticating-to-github/creating-a-personal-access-token) (minimum scopes) as `GITHUB_COM_TOKEN` so that your bot can make authenticated requests to github.com for Changelog retrieval as well as for any dependency that uses GitHub tags.
Without such a token, github.com's API will rate limit requests and make such lookups unreliable.

Finally, you need to decide how your bot should decide which projects to run against.
By default renovate won't find any repo, you need to choose one of the following options for `RENOVATE_EXTRA_FLAGS`.

If you wish for your bot to run against any project which the `RENOVATE_TOKEN` PAT has access to, but which already have a `renovate.json` or similar config file, then add this variable: `RENOVATE_EXTRA_FLAGS=`: `--autodiscover=true`.
This will mean no new projects will be onboarded.

However, we recommend you apply an `autodiscoverFilter` value like the following so that the bot does not run on any stranger's project it gets invited to: `RENOVATE_EXTRA_FLAGS`: `--autodiscover=true --autodiscover-filter=group1/*`.
Checkout renovate [docs](https://docs.renovatebot.com/gitlab-bot-security/) for more information about gitlab security.

If you wish for your bot to run against _every_ project which the `RENOVATE_TOKEN` PAT has access to, and onboard any projects which don't yet have a config, then add this variable: `RENOVATE_EXTRA_FLAGS`: `--autodiscover=true --onboarding=true --autodiscover-filter=group1/*`.

If you wish to manually specify which projects that your bot runs again, then add this variable with a space-delimited set of project names: `RENOVATE_EXTRA_FLAGS`: `group1/repo5 user3/repo1`.

## Configure the Schedule

Add a schedule (`CI / CD` > `Schedules`) to run Renovate regularly.

A good practise is to run it hourly. The following runs Renovate on the third minute every hour: `3 * * * *`.

Because the default pipeline only runs on schedules, you need to use the `play` button of schedule to trigger a manual run.


## Reference

* [Renovate Bot](https://blog.jdriven.com/2022/08/running-renovate-on-gitlab-com/)